﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Content.PM;
using Android.Opengl;
using System.Collections.Generic;
using Android.Gms.Ads;
using MogaMoga;



namespace NesOneAndroid
{


	// the ConfigurationChanges flags set here keep the EGL context
	// from being destroyed whenever the device is rotated or the
	// keyboard is shown (highly recommended for all GL apps)
	[Activity (Label = "My Nes Emulator",
				#if __ANDROID_11__
				HardwareAccelerated=false,
				#endif
		ScreenOrientation = ScreenOrientation.Portrait,
		ConfigurationChanges = ConfigChanges.Orientation | ConfigChanges.KeyboardHidden | ConfigChanges.ScreenSize,
		MainLauncher = true,
		Icon = "@mipmap/ic_launcher")]
	public class MainActivity : Activity
	{
		EmuSurfaceView glSurfaceView;
		RelativeLayout _relative;
		LinearLayout _mainMenu, _notMainMenu;
		Menu _menu;
		AdView _adView;
		Controller _controller;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			Utilities.RootActivity = this;

			Settings.InitializeSettings ();

			this.RequestWindowFeature(WindowFeatures.NoTitle);
			this.Window.AddFlags (WindowManagerFlags.Fullscreen);
			Utilities.SetActivityOrientation (this);

			_relative = new RelativeLayout (this);
			SetContentView (_relative);

			glSurfaceView = new EmuSurfaceView(this);
			glSurfaceView.SystemUiVisibility = StatusBarVisibility.Hidden;

			//glSurfaceView.SetEGLContextClientVersion(2);
			//glSurfaceView.SetRenderer(new EmulatorRenderer());


			_mainMenu = (LinearLayout)View.Inflate (this, Resource.Layout.Main, null);
			_notMainMenu = (LinearLayout)View.Inflate (this, Resource.Layout.not_main_menu, null);


			_relative.AddView (glSurfaceView, new ViewGroup.LayoutParams (ViewGroup.LayoutParams.MatchParent, ViewGroup.LayoutParams.MatchParent));
			_relative.AddView (_mainMenu, new ViewGroup.LayoutParams (ViewGroup.LayoutParams.MatchParent, ViewGroup.LayoutParams.MatchParent));
			_relative.AddView (_notMainMenu, new ViewGroup.LayoutParams (ViewGroup.LayoutParams.MatchParent, ViewGroup.LayoutParams.MatchParent));

			InitScreenControls ();
			DoAdThings ();
			TestToPromptForReview ();

			//_controller = Controller.GetInstance (this);
			//_controller.Init ();
			//var ttt = _controller.GetState(Controller.ActionConnected);
			//Console.WriteLine (ttt);
			//Console.WriteLine (_controller.what	
		}



		public void DoAdThings(){
			var frameLayout = new FrameLayout(this);
			var linearLayout = new LinearLayout(this);

			linearLayout.Orientation = Orientation.Horizontal;
			var linearLayoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FillParent, LinearLayout.LayoutParams.WrapContent);
			linearLayout.LayoutParameters = linearLayoutParams;
			linearLayout.SetGravity(Android.Views.GravityFlags.Left | Android.Views.GravityFlags.Top);

			//frameLayout.AddView(window);

			_adView = new AdView(this);
			_adView.AdUnitId = "ca-app-pub-1442711835182139/1325025404";
			_adView.AdSize = AdSize.Banner;

			linearLayout.AddView (_adView);
			_relative.AddView(linearLayout);
			//frameLayout.AddView(linearLayout);
			//SetContentView(frameLayout);

			try
			{
				#if DEBUG
				var adRequest = new AdRequest.Builder().AddTestDevice("44EC309519A0BD164758C4E1D2B7AEC1").Build();
				_adView.LoadAd(adRequest);
				#else
				var requestbuilder = new AdRequest.Builder();
				_adView.LoadAd(requestbuilder.Build());
				#endif
			}
			catch (Exception ex)
			{
				Console.WriteLine("Test ad creation error.");
			}
		}

		void TestToPromptForReview(){
			var count = Convert.ToInt32(Utilities.Load ("launch_count", "1"));
			++count;
			Utilities.Save ("launch_count", count.ToString ());
			if (count == 15 || count == 36) {
				String str = "https://play.google.com/store/apps/details?id=com.freegamesstudios.nesforandroid";
				this.StartActivity (new Android.Content.Intent (Android.Content.Intent.ActionView, Android.Net.Uri.Parse (str)));
			}
		}

		void InitScreenControls ()
		{
			_notMainMenu.Visibility = ViewStates.Invisible;
			var menuListView = (ListView)FindViewById (Resource.Id.option_list);

			var closeButton = (ImageView)FindViewById (Resource.Id.closeButton1);
			closeButton.Click += (object sender, EventArgs e) =>  {
				HideMainMenu();
				if(Utilities.Renderer!=null){
					Utilities.Renderer.SetSpeed(1);
					Utilities.Renderer.ResumeEmulation();
				}
			};

			var openMenuButton = (ImageView)FindViewById (Resource.Id.openMenuButton);
			openMenuButton.Click += (object sender, EventArgs e) =>  {
				ShowMainMenu();
				if(Utilities.Renderer!=null){
					Utilities.Renderer.PauseEmulation();
				}
			};

			_menu = new Menu (this, menuListView);;
			_menu.LoadMenu ();
		}

		public void HideMainMenu(){
			_mainMenu.Visibility = ViewStates.Invisible;
			_notMainMenu.Visibility = ViewStates.Visible;
		}

		public void ShowMainMenu(){
			_menu.LoadMenu ();
			_mainMenu.Visibility = ViewStates.Visible;
			_notMainMenu.Visibility = ViewStates.Invisible;
		}



		public override bool OnTouchEvent (Android.Views.MotionEvent e)
		{
			glSurfaceView.HandleTouch(e);
			return true;
		}

		protected override void OnDestroy ()
		{
			if (_controller != null) {
				_controller.Exit ();
			}
			glSurfaceView.Destroy ();
			base.OnDestroy ();
		}

		protected override void OnPause ()
		{
			base.OnPause();
			if (_controller != null) {
				_controller.OnPause ();
			}
			if (_adView != null && _adView.Visibility != ViewStates.Gone)
			{
				_adView.Pause();
			}
			glSurfaceView.OnPause ();
		}

		protected override void OnResume ()
		{
			base.OnResume ();
			if (_controller != null) {
				_controller.OnResume ();
			}
			if (_adView != null && _adView.Visibility != ViewStates.Gone)
			{
				_adView.Resume();
			}
			glSurfaceView.OnResume ();
		}

		public override void OnBackPressed ()
		{
			if(Utilities.Renderer != null && Utilities.Renderer.IsROMLoaded){
				Utilities.Renderer.SetSpeed (1);
				Utilities.Renderer.PauseEmulation ();
			}

			AlertDialog.Builder builder;
			builder = new AlertDialog.Builder(this);
			builder.SetTitle("Exit");
			builder.SetMessage("Are you sure you want to quit?");
			builder.SetCancelable(false);
			builder.SetPositiveButton("Yes", delegate { this.Finish(); });
			builder.SetNegativeButton("No", delegate {				
				if(Utilities.Renderer != null && Utilities.Renderer.IsROMLoaded){
					Utilities.Renderer.ResumeEmulation();
				}
			});
			builder.Show();
		}


		public override ComponentName StartService (Intent service)
		{
			string action = service.Action;
			if (!String.IsNullOrEmpty(action) && action.StartsWith("com.bda.controller")) {
			var resolveInfos = this.PackageManager.QueryIntentServices( service, 0 );
				ServiceInfo serviceInfo = resolveInfos[0].ServiceInfo;
				String packageName = serviceInfo.PackageName;
				String className = serviceInfo.Name;
				service.SetComponent( new ComponentName( packageName, className ) );

				service.SetPackage ("com.bda.pivot.mogapgp");
			}
			return base.StartService (service);
		}

		public Controller MogaController{
			get{
				return _controller;
			}
		}
	}



}



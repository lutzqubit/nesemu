﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Widget;
using Android.Views;
using System.IO;

namespace NesOneAndroid
{
	public class CheatEntryAdapter: BaseAdapter<CheatItemModel> {
		List<CheatItemModel> _cheats;
		Activity _context;


		public CheatEntryAdapter (Activity context, List<CheatItemModel> cheats)
			: base()
		{
			this._context = context;
			_cheats = cheats;
			//_cheats.Add (new CheatItemModel{Code = "CheatCode:090:00:00"});

			LoadCheats ();

		}

		public void LoadCheats(){
			var romPrefix = Path.GetFileNameWithoutExtension (Settings.CurrentROMPath);
			var pathToCheatFile = Path.Combine (Settings.CheatsFolder, romPrefix + ".cht");
			using (var stream = File.Open (pathToCheatFile, FileMode.OpenOrCreate, FileAccess.ReadWrite)) {
				var reader = new StreamReader (stream);
				string code = null;
				while ((code = reader.ReadLine ()) != null) {
					if (code == string.Empty) {
						continue;
					}
					if (code.Substring (0, 2) == "S:" || code.Substring (0, 3) == "SC:") {
						_cheats.Add (new CheatItemModel { Code = code.Substring (3) });
					} else {
						_cheats.Add (new CheatItemModel { IsEnabled = true, Code = code.Substring (2) });
					}
				}

			}
		}

		public override long GetItemId(int position)
		{
			return position;
		}
		public override CheatItemModel this[int position]
		{
			get { return _cheats[position]; }
		}
		public override int Count
		{
			get { return _cheats.Count; }
		}
		public List<CheatItemModel> GetCheats(){
			return _cheats;
		}

		public override View GetView(int position, View view, ViewGroup parent)
		{
			var item = _cheats[position];
			if (view == null) {
				view = _context.LayoutInflater.Inflate (Resource.Layout.CheatEntryAdapter, null);
			} else {

			}
			view.FindViewById<TextView>(Resource.Id.cheat_item_code).Text = item.Code;

			var isEnabledToggleButton = view.FindViewById<ToggleButton> (Resource.Id.enabled_cheat);
			isEnabledToggleButton.CheckedChange  -= IsEnabledToggleButton_CheckedChange;
			isEnabledToggleButton.Checked =  item.IsEnabled;
			isEnabledToggleButton.Tag =  item;
			isEnabledToggleButton.CheckedChange  += IsEnabledToggleButton_CheckedChange;



			var isSelectedCheckBox = view.FindViewById<CheckBox> (Resource.Id.select_cheat);
			isSelectedCheckBox.CheckedChange  -= IsSelectedCheckBox_CheckedChange;
			isSelectedCheckBox.Checked =  item.IsSelected;
			isSelectedCheckBox.Tag =  item;
			isSelectedCheckBox.CheckedChange += IsSelectedCheckBox_CheckedChange;



			return view;
		}

		void IsSelectedCheckBox_CheckedChange (object sender, CompoundButton.CheckedChangeEventArgs e)
		{
			var chk = sender as CheckBox;
			var item = chk.Tag as CheatItemModel;
			item.IsSelected = e.IsChecked;
		}


		void IsEnabledToggleButton_CheckedChange (object sender, CompoundButton.CheckedChangeEventArgs e)
		{
			var tog = sender as ToggleButton;
			var item = tog.Tag as CheatItemModel;
			item.IsEnabled = e.IsChecked;
		}

	}




	public class CheatItemModel : Java.Lang.Object {

		public string Code;
		public bool IsEnabled;
		public bool IsSelected;
	}
}


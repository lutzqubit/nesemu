﻿using System;
using Android.Widget;
using Android.App;
using System.Collections;
using Android.Views;
using System.Collections.Generic;
using System.IO;
using Android.Graphics;
using Java.Nio;

namespace NesOneAndroid
{

	public class RecentGameAdapter : BaseAdapter<RecentGameModel> {
		List<RecentGameModel> _items;
		Activity _context;

		public RecentGameAdapter(Activity context)//, List<RecentGameModel> items)
			: base()
		{
			this._context = context;

			List<RecentGameModel> items = new List<RecentGameModel> ();
			foreach (var game in Settings.RecentGames) {
				var rcm = new RecentGameModel { RomPath = game, RomName = System.IO.Path.GetFileNameWithoutExtension(game) };
				items.Add (rcm);
			}
			this._items = items;
		}

		public override long GetItemId(int position)
		{
			return position;
		}
		public override RecentGameModel this[int position]
		{
			get { return _items[position]; }
		}
		public override int Count
		{
			get { return _items.Count; }
		}

		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			var item = _items[position];
			View view = convertView;
			if (view == null) // no view to re-use, create new
				view = _context.LayoutInflater.Inflate(Resource.Layout.RecentGameAdapter, null);

			view.FindViewById<TextView>(Resource.Id.rom_name).Text = item.RomName;
			view.FindViewById<TextView>(Resource.Id.full_path).Text = item.RomPath;
			var filePath = System.IO.Path.Combine (Settings.SnapshotFolder, item.RomName + ".img");
			if (File.Exists(filePath)) {
				view.FindViewById<TextView>(Resource.Id.last_played).Text = "Last Played: " + File.GetLastWriteTime(filePath).ToString();
				var imageBytes = File.ReadAllBytes (filePath);
				var bmp = Bitmap.CreateBitmap(256, 240, Bitmap.Config.Argb8888);
				//
				ByteBuffer dst = ByteBuffer.Wrap(imageBytes);
				bmp.CopyPixelsFromBuffer (dst);

				view.FindViewById<ImageView>(Resource.Id.recent_image).SetImageBitmap(bmp);
			}

			return view;
		}

	}


	public class RecentGameModel
	{
		public string LastPlayedDate;
		public string RomName;
		public string Image;
		public string RomPath;


		public RecentGameModel ()
		{

		}
	}
}


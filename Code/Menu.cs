﻿using System;
using Android.Widget;
using System.Collections.Generic;
using Android.Content;
using Android.Content.Res;
using Android.Views;
using Android.App;

namespace NesOneAndroid
{
	public class Menu
	{
		ListView _menuListView;
		Context _context;

		public Menu (Context context, ListView menu)
		{
			_menuListView = menu;
			_menuListView.ItemClick += ItemClicked;
			_context = context;
		}

		private void ItemClicked (object sender, AdapterView.ItemClickEventArgs e){


			var adapter = _menuListView.Adapter as MenuAdapter;
			var item = adapter [e.Position];

			switch (item.Code) {
			case "load_game":
				Intent loadRomIntent = new Intent(_context, typeof(MyFileListActivity));
				_context.StartActivity (loadRomIntent);
					break;
			case "scan_for_roms":
				Intent searchIntent = new Intent(_context, typeof(SearchForROMsActivity));
				_context.StartActivity (searchIntent);
				break;
			case "recent_games":
				Intent recentIntent = new Intent(_context, typeof(RecentGamesActivity));
				_context.StartActivity (recentIntent);
				break;
			case "save_state":
				Intent saveIntent = new Intent(_context, typeof(SaveStateActivity));
				_context.StartActivity (saveIntent);
				break;
			case "load_state":
				Intent loadIntent = new Intent(_context, typeof(LoadStateActivity));
				_context.StartActivity (loadIntent);
				break;
			case "edit_cheats":
				Intent cheatIntent = new Intent(_context, typeof(CheatActivity));
				_context.StartActivity (cheatIntent);
				break;
			case "settings":
				Intent intent = new Intent(_context, typeof(SettingsActivity));
				_context.StartActivity (intent);
				break;
			case "speed2":
				if(Utilities.Renderer != null && Utilities.Renderer.IsROMLoaded){
					Utilities.Renderer.SetSpeed (2);
					Utilities.RootActivity.HideMainMenu ();
				}
				break;
			case "speed4":
				if(Utilities.Renderer != null && Utilities.Renderer.IsROMLoaded){
					Utilities.Renderer.SetSpeed (4);
					Utilities.RootActivity.HideMainMenu ();
				}
				break;
			case "reset":
				if(Utilities.Renderer != null && Utilities.Renderer.IsROMLoaded){
					Utilities.Renderer.Reset ();
					Utilities.RootActivity.HideMainMenu ();
				}
				break;
			case "quit":
				Quit ();
				break;
			default:
					break;
			}
		}

		private void Quit(){
			AlertDialog.Builder builder;
			builder = new AlertDialog.Builder(_context);
			builder.SetTitle("Exit");
			builder.SetMessage(_context.Resources.GetString (Resource.String.quit_message));
			builder.SetCancelable(false);
			builder.SetPositiveButton("Yes", delegate { Utilities.RootActivity.Finish(); });
			builder.SetNegativeButton("No", delegate {  });
			builder.Show();
		}

		public void LoadMenu(){
			Android.Content.Res.Resources res = _context.Resources;

			List<MenuItem> values = new List<MenuItem> ();

			values.Add (new MenuItem{ Name = res.GetString (Resource.String.load_game), Code = "load_game" });
			values.Add (new MenuItem{ Name = res.GetString (Resource.String.scan_for_roms), Code = "scan_for_roms" });
			values.Add (new MenuItem{ Name = res.GetString (Resource.String.recent_games), Code = "recent_games" });
			//values.Add (new MenuItem{ Name = res.GetString (Resource.String.save_state), Code = "save_state" });
			//values.Add (new MenuItem{ Name = res.GetString (Resource.String.load_state), Code = "load_state" });
			//values.Add (new MenuItem{ Name = res.GetString (Resource.String.reset), Code = "reset" });
			values.Add (new MenuItem{ Name = res.GetString (Resource.String.settings), Code = "settings" });
			values.Add (new MenuItem{ Name = res.GetString (Resource.String.quit), Code = "quit" });

			if (Utilities.Renderer != null & Utilities.Renderer.IsROMLoaded) {
				values.Insert(3, new MenuItem{ Name = res.GetString (Resource.String.load_state), Code = "load_state" });
				values.Insert(3, new MenuItem{ Name = res.GetString (Resource.String.save_state), Code = "save_state" });
				values.Insert(3, new MenuItem{ Name = res.GetString (Resource.String.edit_cheats), Code = "edit_cheats" });
				values.Insert(3, new MenuItem{ Name = res.GetString (Resource.String.speed4), Code = "speed4" });
				values.Insert(3, new MenuItem{ Name = res.GetString (Resource.String.speed2), Code = "speed2" });
				values.Insert(3, new MenuItem{ Name = res.GetString (Resource.String.reset), Code = "reset" });
				//values.Insert(3, res.GetString (Resource.String.save_state));
				//values.Insert(3, res.GetString (Resource.String.reset));
			}
			var adapter = new MenuAdapter((Activity)_context, values);// Resource.Layout.menuitem_layout, Resource.Id.menu_text, 
			_menuListView.Adapter = adapter;
			
		}


	}

	public class MenuAdapter :BaseAdapter<MenuItem>{
		Activity _context;
		List<MenuItem> _items;

		public MenuAdapter(Activity context, List<MenuItem> menuItems)//, List<RecentGameModel> items)
			: base()
		{
			this._context = context;
			_items = menuItems;

		}

		public override long GetItemId(int position)
		{
			return position;
		}
		public override MenuItem this[int position]
		{
			get { return _items[position]; }
		}
		public override int Count
		{
			get { return _items.Count; }
		}


		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			var item = _items[position];
			View view = convertView;
			if (view == null) {
				view = _context.LayoutInflater.Inflate (Resource.Layout.menuitem_layout, null);
			}
			view.FindViewById<TextView>(Resource.Id.menu_text).Text = item.Name;

			return view;
		}

	}

	public class MenuItem{
		public string Name;
		public string Code;
	}
}


﻿using System;
using Android.Widget;
using Android.Views;
using Android.App;
using System.Collections.Generic;
using System.IO;
using Android.Content.Res;

namespace NesOneAndroid
{
	public class SavedStateAdapter: BaseAdapter<SaveStateModel> {

		List<SaveStateModel> _items;
		Activity _context;

		public SavedStateAdapter(Activity context)//, List<RecentGameModel> items)
			: base()
		{
			this._context = context;

			List<SaveStateModel> items = new List<SaveStateModel> ();
			for (int i = 0; i < 4; i++) {
				var item = new SaveStateModel { SlotNumber = (i + 1).ToString() };
				//var filenameOnly = Path.GetFileNameWithoutExtension (Settings.currentROMPath); 
				if (Utilities.Renderer != null) {
					var saveStateFile = Path.Combine (Settings.SaveStateFolder, EmulatorRenderer.GetFileHash(Settings.CurrentROMPath) + item.SlotNumber + ".sav");
					if (System.IO.File.Exists (saveStateFile)) {
						item.SaveDate = _context.Resources.GetString (Resource.String.saved_on) + ": " + File.GetLastWriteTime (saveStateFile).ToString ();
					}
				}
				items.Add (item);
			}
			_items = items;
		}

		public override long GetItemId(int position)
		{
			return position;
		}
		public override SaveStateModel this[int position]
		{
			get { return _items[position]; }
		}
		public override int Count
		{
			get { return _items.Count; }
		}


		public override View GetView(int position, View view, ViewGroup parent)
		{
			var item = _items[position];
			if (view == null) {
				view = _context.LayoutInflater.Inflate (Resource.Layout.SaveStateAdapter, null);
			}
			view.FindViewById<TextView>(Resource.Id.slot_number).Text = "SLOT " + item.SlotNumber;
			view.FindViewById<TextView>(Resource.Id.saved_date).Text =  item.SaveDate;

			return view;
		}

	}

	public class SaveStateModel
	{
		public string SlotNumber;
		public string SaveDate;
	}
}


﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using System.IO;

namespace NesOneAndroid
{
	public class FileListFragment : ListFragment
	{
		public static readonly string DefaultInitialDirectory = "/storage/emulated/0";
		private FileListAdapter _adapter;
		private DirectoryInfo _directory;
		private DirectoryInfo _parentDirectory;
		private DirectoryInfo _dummyParentDirectory = new DirectoryInfo("/..");

		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
			_adapter = new FileListAdapter(Activity, new FileSystemInfo[0]);
			ListAdapter = _adapter;
		}

		public override void OnResume()
		{
			base.OnResume();
			RefreshFilesList(new DirectoryInfo(DefaultInitialDirectory));
		}


		public void RefreshFilesList(FileSystemInfo directory)
		{
			IList<FileSystemInfo> visibleThings = new List<FileSystemInfo>();
			var dir = new DirectoryInfo(directory.FullName);
			try
			{
				//if(_parentDirectory != null){
					visibleThings.Add(_dummyParentDirectory);
				//}
				foreach (var item in dir.GetFileSystemInfos().Where(item => item.IsVisible()))
				{
					if(item.IsDirectory() || (item.IsFile() && (item.Name.EndsWith(".nes") || item.Name.EndsWith(".unf")))) {
						visibleThings.Add(item);
					}
				}
			}
			catch (Exception ex)
			{
				Log.Error("FileListFragment", "Couldn't access the directory " + _directory.FullName + "; " + ex);
				Toast.MakeText(Activity, "Problem retrieving contents of " + directory, ToastLength.Long).Show();
				return;
			}
			_directory = dir;
			// Empty out the adapter and add in the FileSystemInfo objects
			// for the current directory.
			_adapter.Clear();
			_adapter.AddDirectoryContents(visibleThings);
			// If we don't do this, then the ListView will not update itself when then data set
			// in the adapter changes. It will appear to the user that nothing has happened.
			ListView.RefreshDrawableState();
			Log.Verbose("FileListFragment", "Displaying the contents of directory {0}.", directory);
		}

		public override void OnListItemClick(ListView l, View v, int position, long id)
		{
			var fileSystemInfo = _adapter.GetItem(position);
			if (fileSystemInfo == _dummyParentDirectory) {
				if (_directory.Parent == null) {
					return;
				} else {
					fileSystemInfo = _directory.Parent;
				}
			}
			if (fileSystemInfo.IsFile())
			{
				// Do something with the file.  In this case we just pop some toast.
				//Log.Verbose("FileListFragment", "The file {0} was clicked.", fileSystemInfo.FullName);
				//Toast.MakeText(Activity, "You selected file " + fileSystemInfo.FullName, ToastLength.Short).Show();
				Settings.CurrentROMPath = fileSystemInfo.FullName;

				var saveStateFile = System.IO.Path.Combine (Settings.SaveStateFolder, EmulatorRenderer.GetFileHash(Settings.CurrentROMPath) + "0.sav");
				if (File.Exists (saveStateFile)) {

					var context = (Context)this.Activity;
					AlertDialog.Builder builder;
					builder = new AlertDialog.Builder (context);
					builder.SetTitle ("Load ROM");
					builder.SetMessage ("Autosave state from " + File.GetLastWriteTime (saveStateFile).ToString () + " found. Do you want to continue?");
					builder.SetCancelable (false);
					builder.SetPositiveButton ("Yes", delegate {
						LaunchGame (true);
					});
					builder.SetNegativeButton ("No", delegate {
						LaunchGame (false);
					});
					builder.Show ();

				} else {
					LaunchGame (false);
				}


			}
			else
			{
				// Dig into this directory, and display it's contents
				var dir = fileSystemInfo as DirectoryInfo;
				_parentDirectory = dir.Parent;
				RefreshFilesList(fileSystemInfo);
			}
			base.OnListItemClick(l, v, position, id);
		}

		void LaunchGame (bool resume)
		{
			Utilities.Renderer.Init ();
			Utilities.Renderer.RunROM (Settings.CurrentROMPath, resume);
			Utilities.AddRecentGame (Settings.CurrentROMPath);
			Utilities.RootActivity.HideMainMenu ();
			Activity.Finish ();
		}
	}
}


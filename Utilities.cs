﻿using System;
using Android.Views;
using Android.Content;
using System.IO;
using Android.Runtime;
using Android.App;
using System.IO.IsolatedStorage;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Linq;
using Android.Content.PM;
using Android.Graphics;

namespace NesOneAndroid
{
	public enum NesButtons
	{
		JOY_A =  1,
		JOY_B =  2,
		JOY_SELECT  =    4,
		JOY_START     =  8,
		JOY_UP = 0x10,
		JOY_DOWN=  0x20,
		JOY_LEFT=  0x40,
		JOY_RIGHT =      0x80

	}

	public static class Utilities
	{
		public static MainActivity RootActivity{ get; set; }
		public static EmulatorRenderer Renderer{ get; set; }
		//public static Point ScreenSize{ get; set; }

		public static void Save(string key, string value) {
			var sharedPref = Application.Context.GetSharedPreferences("NesOne", FileCreationMode.Private);
			var editor = sharedPref.Edit();

			editor.PutString(key, value.ToString());
			editor.Commit();

		}


		public static string Load(string key, string defaultValue = null) {
			var sharedPref = Application.Context.GetSharedPreferences("NesOne", FileCreationMode.Private);
			string nameValue = sharedPref.GetString(key, defaultValue);
			return nameValue;
		}

		public static void AddRecentGame(string info){
			var file = "recent_games_list";
			if (Settings.RecentGames != null) {
				Settings.RecentGames.Insert (0, info);
				Settings.RecentGames = Settings.RecentGames.Take (5).Distinct().ToList();
			
				using (var storage = IsolatedStorageFile.GetUserStoreForApplication ()) {

					using (var stream = storage.OpenFile (file, FileMode.Create)) {
						XmlSerializer xSer = new XmlSerializer (typeof(List<string>));
						xSer.Serialize (stream, Settings.RecentGames);
					}
				}
			}


		}

		public static void LoadRecentROMList(){
			var file = "recent_games_list";
			using (var storage = IsolatedStorageFile.GetUserStoreForApplication()) {
				try {
					if (storage.FileExists(file)) {
						using (StreamReader reader = new StreamReader(storage.OpenFile(file, FileMode.Open, FileAccess.Read, FileShare.Read))) {
							XmlSerializer xSer = new XmlSerializer(typeof(List<string>));
							Settings.RecentGames = xSer.Deserialize(reader) as List<string>;
						}
					}
				}
				catch(Exception) {
					storage.DeleteFile(file);
					Settings.RecentGames  = new List<string>();
				}
			}
		}

		public static void SetActivityOrientation(Activity activity){
			if (string.Compare (Settings.Orientation, "portrait", true) == 0) {
				activity.RequestedOrientation = ScreenOrientation.Portrait;
			} else {
				activity.RequestedOrientation = ScreenOrientation.Landscape;
			}
		}

		private static Point ParseButtonLocation(string valueToParse){
			try {
				if (valueToParse != null) {
					var strings = valueToParse.Split ('|');
					var point = new Point(Convert.ToInt32(strings[0]),Convert.ToInt32(strings[1]));
					return point;
				}
			} catch {

			}
			return null;
		}

		public static void LoadButtonPositions(Activity activity){
			LoadDefaultButtonPositions (activity);

			Point point = ParseButtonLocation (Load ("ptpD"));
			Settings.DirectionButtonPortraitPosition = (point == null) ? Settings.DirectionButtonPortraitPosition : point;
			point = ParseButtonLocation (Load ("ptpStart"));
			Settings.StartButtonPortraitPosition = (point == null) ? Settings.StartButtonPortraitPosition : point;
			point = ParseButtonLocation (Load ("ptpSelect"));
			Settings.SelectButtonPortraitPosition = (point == null) ? Settings.SelectButtonPortraitPosition : point;
			point = ParseButtonLocation (Load ("ptpA"));
			Settings.AButtonPortraitPosition = (point == null) ? Settings.AButtonPortraitPosition : point;
			point = ParseButtonLocation (Load ("ptpB"));
			Settings.BButtonPortraitPosition = (point == null) ? Settings.BButtonPortraitPosition : point;
			point = ParseButtonLocation (Load ("ptpAB"));
			Settings.ABButtonPortraitPosition = (point == null) ? Settings.ABButtonPortraitPosition : point;


			point = ParseButtonLocation (Load ("ptlD"));
			Settings.DirectionButtonLandscapePosition = (point == null) ? Settings.DirectionButtonLandscapePosition : point;
			point = ParseButtonLocation (Load ("ptlStart"));
			Settings.StartButtonLandscapePosition = (point == null) ? Settings.StartButtonLandscapePosition : point;
			point = ParseButtonLocation (Load ("ptlSelect"));
			Settings.SelectButtonLandscapePosition = (point == null) ? Settings.SelectButtonLandscapePosition : point;
			point = ParseButtonLocation (Load ("ptlA"));
			Settings.AButtonLandscapePosition = (point == null) ? Settings.AButtonLandscapePosition : point;
			point = ParseButtonLocation (Load ("ptlB"));
			Settings.BButtonLandscapePosition = (point == null) ? Settings.BButtonLandscapePosition : point;
			point = ParseButtonLocation (Load ("ptlAB"));
			Settings.ABButtonLandscapePosition = (point == null) ? Settings.ABButtonLandscapePosition : point;
		}


		public static void LoadDefaultButtonPositions(Activity activity){
			Display display = activity.WindowManager.DefaultDisplay;
			Point screenSize = new Point ();
			display.GetSize (screenSize);

			int longSide = 0, shortSide = 0;

			if (screenSize.Y >= screenSize.X) {
				longSide = screenSize.Y;
				shortSide = screenSize.X;
			} else {
				longSide = screenSize.X;
				shortSide = screenSize.Y;
			}

			Settings.DirectionButtonPortraitPosition = new Point (30, longSide - 390); 
			Settings.StartButtonPortraitPosition = new Point ((shortSide / 2) + 50, longSide - 140);
			Settings.SelectButtonPortraitPosition = new Point ((shortSide / 2) - 140, longSide - 140); 
			Settings.AButtonPortraitPosition = new Point (shortSide - 130, longSide - 340);
			Settings.BButtonPortraitPosition = new Point (shortSide - 220, longSide - 200);
			Settings.ABButtonPortraitPosition = new Point (shortSide - 260, longSide - 340);


			Settings.DirectionButtonLandscapePosition = new Point (40, (shortSide / 2)); 
			Settings.StartButtonLandscapePosition = new Point (longSide - 130, 90);
			Settings.SelectButtonLandscapePosition = new Point (50, 90); 
			Settings.AButtonLandscapePosition = new Point (longSide - 130, shortSide - 380);
			Settings.BButtonLandscapePosition = new Point (longSide - 220, shortSide - 240);
			Settings.ABButtonLandscapePosition = new Point (longSide - 260, shortSide - 380);
		}

		public static void LoadMogaKeyMapping(){

		}

		public static LayoutInflater GetLayoutInflater(this Context context)
		{
			return context.GetSystemService(Context.LayoutInflaterService).JavaCast<LayoutInflater>();
		}

		public static bool IsDirectory(this FileSystemInfo fsi)
		{
			if (fsi == null || !fsi.Exists)
			{
				return false;
			}

			return (fsi.Attributes & FileAttributes.Directory) == FileAttributes.Directory;
		}


		public static bool IsFile(this FileSystemInfo fsi)
		{
			if (fsi == null || !fsi.Exists)
			{
				return false;
			}
			return !IsDirectory(fsi);
		}

		public static bool IsVisible(this FileSystemInfo fsi)
		{
			if (fsi == null || !fsi.Exists)
			{
				return false;
			}

			var isHidden = (fsi.Attributes & FileAttributes.Hidden) == FileAttributes.Hidden;
			return !isHidden;
		}
	}
}


﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.IO;
using System.Runtime.InteropServices;

namespace NesOneAndroid
{
	
	[Activity (Label = "Enter GameGenie Cheat Code")]			
	public class CheatActivity : Activity
	{
		bool _saved = true;
		ListView _cheatsListView;


		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView(Resource.Layout.cheat_screen_layout);
			Utilities.SetActivityOrientation (this);

			_cheatsListView = FindViewById<ListView>(Resource.Id.cheats_list_view);
			_cheatsListView.Adapter = new CheatEntryAdapter(this, new List<CheatItemModel>());


			Button addCheatButton = FindViewById<Button>(Resource.Id.add_cheat);
			addCheatButton.Click += AddCheatButton_Click;

			Button saveCheatButton = FindViewById<Button>(Resource.Id.save_cheats);
			saveCheatButton.Click += SaveCheatButton_Click;;

			Button deleteCheatButton = FindViewById<Button>(Resource.Id.delete_cheats);
			deleteCheatButton.Click += DeleteCheatButton_Click;
		}



		void DeleteCheatButton_Click (object sender, EventArgs e)
		{
			var cheatsAdptr = _cheatsListView.Adapter as CheatEntryAdapter;
			var list = cheatsAdptr.GetCheats ();

			var filesToDelete = list.Where(a => a.IsSelected).ToList();
			foreach (var file in filesToDelete) {
				list.Remove(file);
				_saved = false;
			}
			_cheatsListView.InvalidateViews();
		}

		void SaveCheatButton_Click (object sender, EventArgs e)
		{
			var romPrefix = Path.GetFileNameWithoutExtension (Settings.CurrentROMPath);
			var pathToCheatFile = Path.Combine (Settings.CheatsFolder, romPrefix + ".cht");

			var cheatsAdptr = _cheatsListView.Adapter as CheatEntryAdapter;
			var list = cheatsAdptr.GetCheats ();

			using (var stream = File.Open(pathToCheatFile, FileMode.Create, FileAccess.ReadWrite)) {
				var writer = new StreamWriter(stream);
				foreach (var item in list) {
					var prefixText = "S";
					if (!item.Code.Contains("FFFFFFFF")) {
						prefixText += "C"; 
					}
					if (!item.IsEnabled) {
						prefixText += ":"; 
					}
					writer.WriteLine(prefixText + item.Code);
				}
				writer.Flush();
			}
			_saved = true;
		}

		void AddCheatButton_Click (object sender, EventArgs e)
		{
			var error = false;

			var cheatNameField = FindViewById<EditText>(Resource.Id.cheat_name);
			var cheatCodeField = FindViewById<EditText>(Resource.Id.cheat_code);

			if (cheatCodeField.Text.Length == 8 || cheatCodeField.Text.Length == 6) {
				try {
					IntPtr intPointer = TestOpenGL.decode_game_genie_code(cheatCodeField.Text);
					int[] cheatCodeArray = new int[3];
					Marshal.Copy(intPointer, cheatCodeArray, 0, 3);
					TestOpenGL.release_array_memory(intPointer);

					if (cheatCodeArray.Length == 3 && !cheatCodeArray.All(a => a == 0)) {
						var code = string.Empty;
						var subValue = cheatCodeArray[1].ToString("X2");
						if (subValue == "FFFFFFFF") {
							subValue = "FF";
						}
						code = cheatCodeArray[0].ToString("X4") + ":" + subValue + ":" + cheatCodeArray[2].ToString("X2");

						if (cheatNameField.Text == string.Empty) {
							cheatNameField.Text = "CheatCode";
						}
						code += (":" + cheatNameField.Text);
						var cheat = new CheatItemModel { Code = code };

						var cheatsAdptr = _cheatsListView.Adapter as CheatEntryAdapter;
						cheatsAdptr.GetCheats().Insert (0, cheat);
						cheatCodeField.Text = cheatNameField.Text = string.Empty;
						_saved = false;
						_cheatsListView.InvalidateViews();
					}
					else {
						throw new Exception();
					}
				}
				catch {
					error = true;
				}
			}
			else {
				error = true;
			}

			if (error) {
				AlertDialog.Builder builder;
				builder = new AlertDialog.Builder(this);
				builder.SetTitle("Error");
				builder.SetMessage(this.ApplicationContext.Resources.GetString (Resource.String.no_cheat_code_message));
				builder.SetCancelable(false);
				builder.SetPositiveButton("OK", delegate {  });
				builder.Show();
			}
		}


		public override void OnBackPressed ()
		{
			if (_saved) {
				this.Finish ();
			} else {
				AlertDialog.Builder builder;
				builder = new AlertDialog.Builder (this);
				builder.SetTitle ("Exit");
				builder.SetMessage (this.ApplicationContext.Resources.GetString (Resource.String.save_cheats_warning));
				builder.SetCancelable (true);
				builder.SetPositiveButton ("Yes", delegate {
					SaveCheatButton_Click(null, EventArgs.Empty);
					this.Finish ();
				});
				builder.SetNegativeButton ("No", delegate {
					this.Finish ();
				});
				builder.Show ();
			}

		}
	}
}


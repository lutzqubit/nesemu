﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.IO;
using System.Threading.Tasks;


namespace NesOneAndroid
{
	[Activity (Label = "Search all directories")]			
	public class SearchForROMsActivity : Activity
	{
		bool _keepScanning;

		IList<string> _directories;
		IList<string> _files;

		Button _stopButton;
		TextView _currentFolder;
		ListView _listView;

		protected override async void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			_keepScanning = true;
			SetContentView (Resource.Layout.scan_folders_layout);
			Utilities.SetActivityOrientation (this);

			_directories = new List<string> ();
			_files = new List<string> ();

			_listView = (ListView)FindViewById (Resource.Id.rom_list);
			_currentFolder = (TextView)FindViewById (Resource.Id.current_folder);
			_stopButton = (Button)FindViewById (Resource.Id.stop_scan);
			_stopButton.Click += _stopButton_Click;

			await NewMethod ();
		}

		async Task NewMethod ()
		{
			await Task.Run (() => {
				SearchDirectory ("/storage/emulated/0");
				SearchDirectory (Android.OS.Environment.ExternalStorageDirectory.Path);
			});


			_currentFolder.Text = string.Empty;
			_stopButton.Visibility = ViewStates.Invisible;
			_listView.Adapter = new ArrayAdapter<string> (this, Resource.Layout.scan_folders_item_layout, _files);
			_listView.ItemClick += _listView_ItemClick;
		}

		void _stopButton_Click (object sender, EventArgs e)
		{
			_keepScanning = false;
		}

		void SearchDirectory(string sDir) 
		{
			try	
			{
				foreach (string d in Directory.GetDirectories(sDir)) 
				{
					if(_keepScanning){
						foreach (string f in Directory.GetFiles(d, "*.*").Where(s => s.EndsWith(".nes") || s.EndsWith(".unf"))) 
						{
							if(!_files.Select(a => Path.GetFileName(a)).Contains(Path.GetFileName(f))){
								_files.Add(f);
							}
						}
						SearchDirectory(d);
						this.RunOnUiThread(() =>{
							_currentFolder.Text = d;
						});
					}
				}
				
			}
			catch (Exception e) 
			{
				Console.WriteLine(e.Message);
			}
		}

		void _listView_ItemClick (object sender, AdapterView.ItemClickEventArgs e)
		{
			Settings.CurrentROMPath = _files[e.Position];

			var saveStateFile = System.IO.Path.Combine (Settings.SaveStateFolder, EmulatorRenderer.GetFileHash(Settings.CurrentROMPath) + "0.sav");
			if (File.Exists (saveStateFile)) {

				//var context = (Context)this.Activity;
				AlertDialog.Builder builder;
				builder = new AlertDialog.Builder (this);
				builder.SetTitle ("Load ROM");
				builder.SetMessage ("Autosave state from " + File.GetLastWriteTime (saveStateFile).ToString () + " found. Do you want to continue?");
				builder.SetCancelable (false);
				builder.SetPositiveButton ("Yes", delegate {
					LaunchGame (true);
				});
				builder.SetNegativeButton ("No", delegate {
					LaunchGame (false);
				});
				builder.Show ();

			} else {
				LaunchGame (false);
			}

		}

		void LaunchGame (bool resume)
		{
			Utilities.Renderer.Init ();
			Utilities.Renderer.RunROM (Settings.CurrentROMPath, resume);
			Utilities.AddRecentGame (Settings.CurrentROMPath);
			Utilities.RootActivity.HideMainMenu ();
			this.Finish ();
		}
	}
}


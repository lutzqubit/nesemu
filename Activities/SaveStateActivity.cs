﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace NesOneAndroid
{
	[Activity (Label = "Save Game State")]			
	public class SaveStateActivity : Activity
	{
		ListView _listView;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView(Resource.Layout.save_state_layout);
			Utilities.SetActivityOrientation (this);
			_listView = FindViewById<ListView>(Resource.Id.save_state_list);
			_listView.Adapter = new SavedStateAdapter(this);
			_listView.ItemClick += OnListItemClick; 

		}

		void OnListItemClick(object sender, AdapterView.ItemClickEventArgs e)
		{
			var listView = sender as ListView;
			var adapter = listView.Adapter as SavedStateAdapter;
			var item = adapter [e.Position];

			Utilities.Renderer.SaveState (item.SlotNumber);
			Utilities.RootActivity.HideMainMenu ();
			if(Utilities.Renderer!=null){
				Utilities.Renderer.ResumeEmulation();
			}
			this.Finish ();
		}
	}
}


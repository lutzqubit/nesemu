﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Content.PM;

namespace NesOneAndroid
{
	[Activity (Label = "Settings")]			
	public class SettingsActivity : Activity
	{
		CheckBox _autodetectChk;
		CheckBox _soundOnChk;
		CheckBox _abButtonChk;
		Button _buttonLayout;

		Spinner _orientation;
		Spinner _buttonStyle;
		Spinner _buttonSize;
		Spinner _sampleRate;
		Spinner _videoSystem;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView(Resource.Layout.settings_layout);
			Utilities.SetActivityOrientation (this);
			InitViews ();
		}

		void InitViews(){
			_autodetectChk = (CheckBox)FindViewById (Resource.Id.autodetect);
			_autodetectChk.Click += AutoDetectClicked;
			_autodetectChk.Checked = Settings.VideoAutodetect;

			_soundOnChk = (CheckBox)FindViewById (Resource.Id.sound_on);
			_soundOnChk.Click += SoundOnClicked;
			_soundOnChk.Checked = Settings.SoundOn;

			_abButtonChk = (CheckBox)FindViewById (Resource.Id.ab_button);
			_abButtonChk.Click += EnableABButtonClicked;
			_abButtonChk.Checked = Settings.ABButtonEnabled;

			_orientation = (Spinner)FindViewById (Resource.Id.orientation);
			var orientations = new List<string> { "Portrait", "Landscape" };
			var oAdapter = new ArrayAdapter<string> (this, Android.Resource.Layout.SimpleSpinnerItem, orientations);
			oAdapter.SetDropDownViewResource(Resource.Layout.spinner_layout_one);
			_orientation.Adapter = oAdapter;
			_orientation.SetSelection (orientations.FindIndex (a => String.Compare(Settings.Orientation, a, true) == 0));
			_orientation.ItemSelected += OrientationSelected;


			_buttonSize = (Spinner)FindViewById (Resource.Id.select_button_size);
			var buttonSizes = new List<string> { "Small", "Default" , "Large" };
			var bzAdapter = new ArrayAdapter<string> (this, Android.Resource.Layout.SimpleSpinnerItem, buttonSizes);
			bzAdapter.SetDropDownViewResource(Resource.Layout.spinner_layout_one);
			_buttonSize.Adapter = bzAdapter;
			_buttonSize.SetSelection (buttonSizes.FindIndex (a => String.Compare(Settings.ButtonSize, a, true) == 0));
			_buttonSize.ItemSelected += ButtonSizeSelected;

			_buttonStyle = (Spinner)FindViewById (Resource.Id.select_button_style);
			var buttonStyles = new List<string> { "Default", "Style 1" , "Style 2" };
			var bsAdapter = new ArrayAdapter<string> (this, Android.Resource.Layout.SimpleSpinnerItem, buttonStyles);
			bsAdapter.SetDropDownViewResource(Resource.Layout.spinner_layout_one);
			_buttonStyle.Adapter = bsAdapter;
			_buttonStyle.SetSelection (buttonStyles.FindIndex (a => String.Compare(Settings.ButtonStyle, a, true) == 0));
			_buttonStyle.ItemSelected += ButtonStyleSelected;


			_sampleRate = (Spinner)FindViewById (Resource.Id.select_sample_rate);
			var sampleRates = new List<string> { "22050", "44100" };
			var srAdapter = new ArrayAdapter<string> (this, Android.Resource.Layout.SimpleSpinnerItem, sampleRates);
			srAdapter.SetDropDownViewResource(Resource.Layout.spinner_layout_one);
			_sampleRate.Adapter = srAdapter;
			_sampleRate.SetSelection (sampleRates.FindIndex (a => String.Compare(Settings.SampleRate, a, true) == 0));
			_sampleRate.ItemSelected += SampleRateSelected;

			_videoSystem = (Spinner)FindViewById (Resource.Id.select_video_system);
			var videoSystems = new List<string> { "PAL", "NTSC" };
			var vsAdapter = new ArrayAdapter<string> (this, Android.Resource.Layout.SimpleListItem1, videoSystems);
			vsAdapter.SetDropDownViewResource(Resource.Layout.spinner_layout_one);
			_videoSystem.Adapter = vsAdapter;
			_videoSystem.SetSelection (videoSystems.FindIndex (a => String.Compare(Settings.VideoSystem, a, true) == 0));
			_videoSystem.ItemSelected += VideoSystemSelected;


			_buttonLayout = (Button)FindViewById (Resource.Id.change_layout);
			_buttonLayout.Click += _buttonLayout_Click;
		}

		void _buttonLayout_Click (object sender, EventArgs e)
		{
			Intent intent = new Intent(this, typeof(ControllerLayoutActivity));
			intent.SetFlags (ActivityFlags.NoHistory);
			StartActivity (intent);
		}

		void ButtonSizeSelected (object sender, AdapterView.ItemSelectedEventArgs e)
		{
			Settings.ButtonSize =_buttonSize.SelectedItem.ToString ();
			Utilities.Save (Settings.SettingsButtonSize, Settings.ButtonSize);

		}

		void SampleRateSelected (object sender, AdapterView.ItemSelectedEventArgs e)
		{
			Settings.SampleRate =_sampleRate.SelectedItem.ToString ();
			Utilities.Save (Settings.SettingsSampleRate, Settings.SampleRate);
		}

		void ButtonStyleSelected (object sender, AdapterView.ItemSelectedEventArgs e)
		{
			Settings.ButtonStyle =_buttonStyle.SelectedItem.ToString ();
			Utilities.Save (Settings.SettingsButtonStyle, Settings.ButtonStyle);
		}

		void VideoSystemSelected (object sender, AdapterView.ItemSelectedEventArgs e)
		{
			Settings.VideoSystem =_videoSystem.SelectedItem.ToString ();
			Utilities.Save (Settings.SettingsVideoSystem, Settings.VideoSystem);
		}

		void OrientationSelected (object sender, AdapterView.ItemSelectedEventArgs e)
		{
			Settings.Orientation =_orientation.SelectedItem.ToString ();
			Utilities.Save (Settings.SettingsOrientation, Settings.Orientation);
			Utilities.SetActivityOrientation (this);
		}

		void EnableABButtonClicked (object sender, EventArgs e)
		{
			Settings.ABButtonEnabled = _abButtonChk.Checked;
			Utilities.Save (Settings.SettingsABButtonEnabled, _abButtonChk.Checked.ToString ());
		}

		void SoundOnClicked (object sender, EventArgs e)
		{
			Settings.SoundOn = _soundOnChk.Checked;
			Utilities.Save (Settings.SettingsSoundOn, _soundOnChk.Checked.ToString ());

		}

		void AutoDetectClicked (object sender, EventArgs e)
		{
			Settings.VideoAutodetect = _autodetectChk.Checked;
			Utilities.Save (Settings.SettingsAutodetect, _autodetectChk.Checked.ToString ());
		}

		public override void OnBackPressed ()
		{
			UpdateOrientation ();
			EmulatorRenderer.InitializeEmulatorSettings ();
			this.Finish();
		}

		void UpdateOrientation(){
			Utilities.SetActivityOrientation (Utilities.RootActivity);
		}
	}
}


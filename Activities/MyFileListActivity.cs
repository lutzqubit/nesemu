﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.IO;
using Android.Support.V4.App;


namespace NesOneAndroid
{
	[Activity (Label = "@string/app_name")]			
	public class MyFileListActivity : FragmentActivity
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			var layout = (FrameLayout)View.Inflate (this, Resource.Layout.file_picker_main, null);
			SetContentView(layout);
			Utilities.SetActivityOrientation (this);
			//SetContentView(Resource.Layout.file_picker_main);
		}
	}
}


﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.IO;

namespace NesOneAndroid
{
	[Activity (Label = "Recent Games")]			
	public class RecentGamesActivity : Activity
	{
		ListView _listView;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView(Resource.Layout.recent_game_layout);
			Utilities.SetActivityOrientation (this);

			_listView = FindViewById<ListView>(Resource.Id.recent_list);
			_listView.Adapter = new RecentGameAdapter(this);
			_listView.ItemClick += OnListItemClick; 
			// Create your application here
		}

		void OnListItemClick(object sender, AdapterView.ItemClickEventArgs e)
		{
			var listView = sender as ListView;
			var adapter = listView.Adapter as RecentGameAdapter;
			var item = adapter [e.Position];

			if (File.Exists (item.RomPath)) {
				Settings.CurrentROMPath = item.RomPath;
				Utilities.Renderer.Init ();

				var saveStateFile = System.IO.Path.Combine (Settings.SaveStateFolder, EmulatorRenderer.GetFileHash(Settings.CurrentROMPath) + "0.sav");
				if (File.Exists (saveStateFile)) {

					var context = (Context)this;
					AlertDialog.Builder builder;
					builder = new AlertDialog.Builder (context);
					builder.SetTitle ("Load ROM");
					builder.SetMessage ("An Autosave file from " + File.GetLastWriteTime (saveStateFile).ToString () + " was found. Do you want to load it?");
					builder.SetCancelable (false);
					builder.SetPositiveButton ("Yes", delegate {
						LaunchGame (true);
					});
					builder.SetNegativeButton ("No", delegate {
						LaunchGame (false);
					});
					builder.Show ();

				} else {
					LaunchGame (false);
				}
					
			} else {
				AlertDialog.Builder builder;
				builder = new AlertDialog.Builder(this);
				builder.SetTitle("Error");
				builder.SetMessage(this.ApplicationContext.Resources.GetString (Resource.String.bad_file_link));
				builder.SetCancelable(false);
				builder.SetPositiveButton("OK", delegate {  });
				builder.Show();
			}
		}

		void LaunchGame (bool resume)
		{
			Utilities.Renderer.RunROM (Settings.CurrentROMPath, resume);
			Utilities.AddRecentGame (Settings.CurrentROMPath);
			Utilities.RootActivity.HideMainMenu ();
			this.Finish ();
		}
	}


}


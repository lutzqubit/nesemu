﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;

namespace NesOneAndroid
{
	[Activity (Label = "Virtual Controller Layout", NoHistory = true)]			
	public class ControllerLayoutActivity : Activity
	{
		float _buttonScale;
		ImageView _selectedImageView;
		PointF _imageViewPosition;
		int _pointerID;


		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			this.RequestWindowFeature(WindowFeatures.NoTitle);
			this.Window.AddFlags (WindowManagerFlags.Fullscreen);

			SetContentView(Resource.Layout.controller_layout);
			Utilities.SetActivityOrientation (this);

		}


		protected override void OnResume ()
		{
			// never forget to do this!
			base.OnResume ();
			AddResetButton ();
			CreateButtons ();
		}

	

		void AddResetButton ()
		{
			Button resetButton = new Button (this);
			resetButton.Text = this.Resources.GetString (Resource.String.reset);
			resetButton.Click += ResetButton_Click;
			RelativeLayout.LayoutParams prams = new RelativeLayout.LayoutParams (ViewGroup.LayoutParams.WrapContent, ViewGroup.LayoutParams.WrapContent);
			prams.TopMargin = 30;
			prams.LeftMargin = 30;
			RelativeLayout layout = (RelativeLayout)FindViewById (Resource.Id.mycontrollerlayout);
			layout.AddView (resetButton, prams);
		}

		private void CreateButtons ()
		{
			switch (Settings.ButtonSize.ToLower ()) {
			case "small":
				_buttonScale = 0.75f;
				break;
			case "large":
				_buttonScale = 1.2f;
				break;
			default:
				_buttonScale = 1.0f;
				break;
			}



			Utilities.LoadButtonPositions (this);

			if (string.Compare (Settings.Orientation, "portrait", true) == 0) {
				if (Settings.ABButtonEnabled) {
					createBitmap ("buttons/" + Settings.ButtonStyle.ToLower () + "/ab.png", Settings.ABButtonPortraitPosition.X, Settings.ABButtonPortraitPosition.Y, "ptpAB");
				}

				createBitmap ("buttons/" + Settings.ButtonStyle.ToLower () + "/a.png", Settings.AButtonPortraitPosition.X, Settings.AButtonPortraitPosition.Y, "ptpA");
				createBitmap ("buttons/" + Settings.ButtonStyle.ToLower () + "/b.png", Settings.BButtonPortraitPosition.X, Settings.BButtonPortraitPosition.Y, "ptpB");
				createBitmap ("buttons/" + Settings.ButtonStyle.ToLower () + "/start.png", Settings.StartButtonPortraitPosition.X, Settings.StartButtonPortraitPosition.Y, "ptpStart");
				createBitmap ("buttons/" + Settings.ButtonStyle.ToLower () + "/select.png", Settings.SelectButtonPortraitPosition.X, Settings.SelectButtonPortraitPosition.Y, "ptpSelect");
				createBitmap ("buttons/" + Settings.ButtonStyle.ToLower () + "/direction.png", Settings.DirectionButtonPortraitPosition.X, Settings.DirectionButtonPortraitPosition.Y, "ptpD");
			}else {

				if (Settings.ABButtonEnabled) {
					createBitmap ("buttons/" + Settings.ButtonStyle.ToLower () + "/ab.png", Settings.ABButtonLandscapePosition.X, Settings.ABButtonLandscapePosition.Y, "ptlAB");
				}

				createBitmap ("buttons/" + Settings.ButtonStyle.ToLower () + "/a.png", Settings.AButtonLandscapePosition.X, Settings.AButtonLandscapePosition.Y, "ptlA");
				createBitmap ("buttons/" + Settings.ButtonStyle.ToLower () + "/b.png", Settings.BButtonLandscapePosition.X, Settings.BButtonLandscapePosition.Y, "ptlB");
				createBitmap ("buttons/" + Settings.ButtonStyle.ToLower () + "/start.png", Settings.StartButtonLandscapePosition.X, Settings.StartButtonLandscapePosition.Y, "ptlStart");
				createBitmap ("buttons/" + Settings.ButtonStyle.ToLower () + "/select.png", Settings.SelectButtonLandscapePosition.X, Settings.SelectButtonLandscapePosition.Y, "ptlSelect");
				createBitmap ("buttons/" + Settings.ButtonStyle.ToLower () + "/direction.png", Settings.DirectionButtonLandscapePosition.X, Settings.DirectionButtonLandscapePosition.Y, "ptlD");
			}



		}



		void ResetButton_Click (object sender, EventArgs e)
		{
			Utilities.LoadDefaultButtonPositions (this);
			RelativeLayout layout = (RelativeLayout) FindViewById(Resource.Id.mycontrollerlayout);
			layout.RemoveAllViews ();
			AddResetButton ();
			if (string.Compare (Settings.Orientation, "portrait", true) == 0) {
				if (Settings.ABButtonEnabled) {
					createBitmap ("buttons/" + Settings.ButtonStyle.ToLower () + "/ab.png", Settings.ABButtonPortraitPosition.X, Settings.ABButtonPortraitPosition.Y, "ptpAB");
				}

				createBitmap ("buttons/" + Settings.ButtonStyle.ToLower () + "/a.png", Settings.AButtonPortraitPosition.X, Settings.AButtonPortraitPosition.Y, "ptpA");
				createBitmap ("buttons/" + Settings.ButtonStyle.ToLower () + "/b.png", Settings.BButtonPortraitPosition.X, Settings.BButtonPortraitPosition.Y, "ptpB");
				createBitmap ("buttons/" + Settings.ButtonStyle.ToLower () + "/start.png", Settings.StartButtonPortraitPosition.X, Settings.StartButtonPortraitPosition.Y, "ptpStart");
				createBitmap ("buttons/" + Settings.ButtonStyle.ToLower () + "/select.png", Settings.SelectButtonPortraitPosition.X, Settings.SelectButtonPortraitPosition.Y, "ptpSelect");
				createBitmap ("buttons/" + Settings.ButtonStyle.ToLower () + "/direction.png", Settings.DirectionButtonPortraitPosition.X, Settings.DirectionButtonPortraitPosition.Y, "ptpD");
			}else {

				if (Settings.ABButtonEnabled) {
					createBitmap ("buttons/" + Settings.ButtonStyle.ToLower () + "/ab.png", Settings.ABButtonLandscapePosition.X, Settings.ABButtonLandscapePosition.Y, "ptlAB");
				}

				createBitmap ("buttons/" + Settings.ButtonStyle.ToLower () + "/a.png", Settings.AButtonLandscapePosition.X, Settings.AButtonLandscapePosition.Y, "ptlA");
				createBitmap ("buttons/" + Settings.ButtonStyle.ToLower () + "/b.png", Settings.BButtonLandscapePosition.X, Settings.BButtonLandscapePosition.Y, "ptlB");
				createBitmap ("buttons/" + Settings.ButtonStyle.ToLower () + "/start.png", Settings.StartButtonLandscapePosition.X, Settings.StartButtonLandscapePosition.Y, "ptlStart");
				createBitmap ("buttons/" + Settings.ButtonStyle.ToLower () + "/select.png", Settings.SelectButtonLandscapePosition.X, Settings.SelectButtonLandscapePosition.Y, "ptlSelect");
				createBitmap ("buttons/" + Settings.ButtonStyle.ToLower () + "/direction.png", Settings.DirectionButtonLandscapePosition.X, Settings.DirectionButtonLandscapePosition.Y, "ptlD");
			}

			for (int i=0; i < layout.ChildCount; i++){
				var v = layout.GetChildAt(i) as ImageView;
				if (v != null) {
					var prams = v.LayoutParameters as RelativeLayout.LayoutParams;
					Utilities.Save (v.Tag.ToString (), prams.LeftMargin.ToString () + "|" + prams.TopMargin);
				}
			}

		} 

		private void createBitmap(string filePath, float x, float y, string saveDataKey){
			var context = Android.App.Application.Context;
			var bitmap = BitmapFactory.DecodeStream (context.Assets.Open (filePath));

			int width = Convert.ToInt32 ((float)bitmap.Width * _buttonScale);
			int height = Convert.ToInt32 ((float)bitmap.Height * _buttonScale);

			bitmap = Bitmap.CreateScaledBitmap (bitmap, width, height, false);
			var imageBytes = new int[bitmap.Width * bitmap.Height];
			bitmap.GetPixels (imageBytes, 0, bitmap.Width, 0, 0, bitmap.Width, bitmap.Height);

			ImageView a = new ImageView (this);
			a.SetImageBitmap (bitmap);
			RelativeLayout layout = (RelativeLayout) FindViewById(Resource.Id.mycontrollerlayout);
			RelativeLayout.LayoutParams prams = new RelativeLayout.LayoutParams(bitmap.Width, bitmap.Height);
			prams.LeftMargin =(int) x;
			prams.TopMargin = (int) y;
			a.Touch += ImageTouched;
			a.Tag = saveDataKey;
			layout.AddView(a, prams);


		}



		void ImageTouched (object sender, View.TouchEventArgs e)
		{
			var imageView = sender as ImageView;
			if (imageView == null) {
				return;
			}

			if (e.Event.Action == MotionEventActions.Down && e.Event.Action != MotionEventActions.PointerDown && _selectedImageView == null) {
				_selectedImageView = imageView;
				_imageViewPosition = new PointF(e.Event.GetX(), e.Event.GetY());
				var pointerIndex = e.Event.ActionIndex;			
				_pointerID = e.Event.GetPointerId (pointerIndex);
			}

			if (e.Event.Action == MotionEventActions.Move && _selectedImageView != null) {

				var pointCount = e.Event.PointerCount;
				for (int i = 0; i < pointCount; i++) {
					int movePointerId = e.Event.GetPointerId (i);
					if (movePointerId != _pointerID) {
						return;
					}
					Display display = WindowManager.DefaultDisplay;
					Point screenSize = new Point ();
					display.GetSize (screenSize);

				
					var newPoint = new PointF (_imageViewPosition.X - e.Event.GetX (), _imageViewPosition.Y - e.Event.GetY ());
					var prams = _selectedImageView.LayoutParameters as RelativeLayout.LayoutParams;
					prams.LeftMargin = prams.LeftMargin - (int)newPoint.X;
					if (prams.LeftMargin < 0) {
						prams.LeftMargin = 0;
					}
					if (prams.LeftMargin > (screenSize.X - prams.Width)) {
						prams.LeftMargin = screenSize.X - prams.Width;
					}

					prams.TopMargin = prams.TopMargin - (int)newPoint.Y;
					if (prams.TopMargin < 0) {
						prams.TopMargin = 0;
					}
					if (prams.TopMargin > (screenSize.Y - prams.Height)) {
						prams.TopMargin = screenSize.Y - prams.Height;
					}
					_selectedImageView.RequestLayout ();	
				}
			}

			if ((e.Event.Action == MotionEventActions.Cancel || e.Event.Action == MotionEventActions.Up) && e.Event.GetPointerId(e.Event.ActionIndex) == _pointerID) {
				var prams = _selectedImageView.LayoutParameters as RelativeLayout.LayoutParams;
				Utilities.Save (_selectedImageView.Tag.ToString (), prams.LeftMargin.ToString () + "|" + prams.TopMargin);
				_selectedImageView = null;
			}
		}


		public override void OnBackPressed ()
		{
			this.Finish ();
		}

	}
}


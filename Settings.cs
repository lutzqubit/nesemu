﻿using System;
using System.Collections.Generic;
using System.IO.IsolatedStorage;
using System.IO;
using Android.Graphics;

namespace NesOneAndroid
{
	public static class Settings
	{
		public static readonly string SettingsABButtonEnabled = "ABButton";
		public static readonly string SettingsSoundOn = "Sound";
		public static readonly string SettingsAutodetect = "Autodetect";

		public static readonly string SettingsVideoSystem = "VideoSystem";
		public static readonly string SettingsOrientation = "Orientation";
		public static readonly string SettingsSampleRate = "SampleRate";
		public static readonly string SettingsButtonStyle = "ButtonStyle";
		public static readonly string SettingsButtonSize = "ButtonSize";

		public static bool VideoAutodetect;
		public static bool SoundOn;
		public static bool ABButtonEnabled;

		public static string Orientation;
		public static string VideoSystem;
		public static string SampleRate;
		public static string ButtonStyle;
		public static string ButtonSize;


		public static Point AButtonPortraitPosition;
		public static Point BButtonPortraitPosition;
		public static Point ABButtonPortraitPosition;
		public static Point StartButtonPortraitPosition;
		public static Point SelectButtonPortraitPosition;
		public static Point DirectionButtonPortraitPosition;

		public static Point AButtonLandscapePosition;
		public static Point BButtonLandscapePosition;
		public static Point ABButtonLandscapePosition;
		public static Point StartButtonLandscapePosition;
		public static Point SelectButtonLandscapePosition;
		public static Point DirectionButtonLandscapePosition;


		public static string CurrentROMSnapshotPath = "";
		public static string CurrentROMPath = "";


		public static string AppRootPath;
		public static string SaveStateFolder = "";
		public static string SnapshotFolder = "";
		public static string CheatsFolder = "";


		public static Dictionary<string, int> MogaKeys = new Dictionary<string, int> ();


		public static List<string> RecentGames = new List<string>();


		public static void InitializeSettings(){
			var path = Environment.GetFolderPath (Environment.SpecialFolder.Personal);

			var savePath = System.IO.Path.Combine (path, "saves");
			Directory.CreateDirectory (savePath);
			SaveStateFolder = savePath;

			var snapPath = System.IO.Path.Combine (path, "snaps");
			Directory.CreateDirectory (snapPath);
			SnapshotFolder = snapPath;

			var cheatsPath = System.IO.Path.Combine (path, "cheats");
			Directory.CreateDirectory (cheatsPath);
			CheatsFolder = cheatsPath;
			/*using (var storage = IsolatedStorageFile.GetUserStoreForApplication ()) {
				if (!storage.DirectoryExists ("saves")) {
					storage.CreateDirectory ("saves");
				}
			}*/

			VideoAutodetect = Convert.ToBoolean(Utilities.Load(Settings.SettingsAutodetect, "true"));
			SoundOn = Convert.ToBoolean(Utilities.Load(Settings.SettingsSoundOn, "true"));
			ABButtonEnabled = Convert.ToBoolean(Utilities.Load(Settings.SettingsABButtonEnabled, "true"));

			Orientation = Utilities.Load(Settings.SettingsOrientation, "Portrait");
			VideoSystem = Utilities.Load(Settings.SettingsVideoSystem, "NTSC");
			SampleRate = Utilities.Load(Settings.SettingsSampleRate, "22050");
			ButtonStyle = Utilities.Load(Settings.SettingsButtonStyle, "default");
			ButtonSize = Utilities.Load(Settings.SettingsButtonSize, "default");

			Utilities.LoadRecentROMList ();

		}
	}
}


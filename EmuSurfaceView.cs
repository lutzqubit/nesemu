﻿using System;
//using Android.Opengl;
using Android.Content;
using Javax.Microedition.Khronos.Egl;
using Android.Util;
//using Android.Graphics;
using Android.Graphics;
using Android.Views;
using Android.Widget;


namespace NesOneAndroid
{
	public class EmuSurfaceView : Android.Opengl.GLSurfaceView 
	{
		public static String TAG = "EmuSurfaceView";

		EmulatorRenderer _renderer;

		public EmuSurfaceView(Context context):base(context) {
			Init(false, 0, 0);
		}


		private void Init(bool translucent, int depth, int stencil) {

			/* By default, GLSurfaceView() creates a RGB_565 opaque surface.
         * If we want a translucent one, we should change the surface's
         * format here, using PixelFormat.TRANSLUCENT for GL Surfaces
         * is interpreted as any 32-bit surface with alpha by SurfaceFlinger.
         */
			if (translucent) {
				this.Holder.SetFormat(Format.Translucent);
			}

			/* Setup the context factory for 2.0 rendering.
         * See ContextFactory class definition below
         */
			SetEGLContextFactory(new ContextFactory());

			/* We need to choose an EGLConfig that matches the format of
         * our surface exactly. This is going to be done in our
         * custom config chooser. See ConfigChooser class definition
         * below.
         */
			SetEGLConfigChooser( translucent ?
				new MyConfigChooser(8, 8, 8, 8, depth, stencil) :
				new MyConfigChooser(5, 6, 5, 0, depth, stencil) );


			LinearLayout linearLayout= new LinearLayout(this.Context);
			linearLayout.Orientation = Orientation.Vertical;

			linearLayout.LayoutParameters = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MatchParent, LinearLayout.LayoutParams.MatchParent);

			ImageView menuButton = new ImageView (this.Context);
			menuButton.SetImageResource (Resource.Drawable.close);
			menuButton.Top = 20;
			menuButton.Right = 20;

			linearLayout.AddView (menuButton);


			/* Set the renderer responsible for frame rendering */
			_renderer = new EmulatorRenderer ();
			Utilities.Renderer = _renderer;
			SetRenderer(_renderer);
		}


		class ContextFactory: Java.Lang.Object,Android.Opengl.GLSurfaceView.IEGLContextFactory {
			private static int EGL_CONTEXT_CLIENT_VERSION = 0x3098;
			public EGLContext CreateContext(IEGL10 egl, EGLDisplay display, EGLConfig eglConfig) {

				CheckEglError("Before eglCreateContext", egl);
				int[] attrib_list = {EGL_CONTEXT_CLIENT_VERSION, 2, EGL10.EglNone };
				EGLContext context = egl.EglCreateContext(display, eglConfig, EGL10.EglNoContext, attrib_list);
				CheckEglError("After eglCreateContext", egl);
				return context;
			}

			public void DestroyContext(IEGL10 egl, Javax.Microedition.Khronos.Egl.EGLDisplay display, Javax.Microedition.Khronos.Egl.EGLContext context) {
				egl.EglDestroyContext(display, context);
			}
		}

		private static void CheckEglError(String prompt, IEGL10 egl) {
			int error;
			while ((error = egl.EglGetError()) != EGL10.EglSuccess) {
				Log.Error(TAG, String.Format("%s: EGL error: 0x%x", prompt, error));
			}
		}

		public override void OnPause ()	{
			base.OnPause ();
			if (_renderer != null) {
				_renderer.Pause ();
			}
		}

		public override void OnResume (){
			base.OnResume ();
			if (_renderer != null) {
				_renderer.Resume ();
			}
		}

		public void Destroy(){
			_renderer.Destroy ();
		}


		public void HandleTouch(MotionEvent e){
			_renderer.HandleTouch (e);
		}

		public EmulatorRenderer GetRenderer(){
			return _renderer;
		}
	}



	class MyConfigChooser: Java.Lang.Object,Android.Opengl.GLSurfaceView.IEGLConfigChooser {

		public MyConfigChooser(int r, int g, int b, int a, int depth, int stencil) {
			mRedSize = r;
			mGreenSize = g;
			mBlueSize = b;
			mAlphaSize = a;
			mDepthSize = depth;
			mStencilSize = stencil;
		}

		/* This EGL config specification is used to specify 2.0 rendering.
         * We use a minimum size of 4 bits for red/green/blue, but will
         * perform actual matching in chooseConfig() below.
         */
		private static int EGL_OPENGL_ES2_BIT = 4;
		private static int[] s_configAttribs2 =
		{
			EGL10.EglRedSize, 4,
			12323, 4,
			EGL10.EglBlueSize, 4,
			EGL10.EglRenderableType, EGL_OPENGL_ES2_BIT,
			EGL10.EglNone
		};

		public EGLConfig ChooseConfig(IEGL10 egl, EGLDisplay display) {

			/* Get the number of minimally matching EGL configurations
             */
			int[] num_config = new int[1];
			egl.EglChooseConfig(display, s_configAttribs2, null, 0, num_config);

			int numConfigs = num_config[0];

			if (numConfigs <= 0) {
				throw new Java.Lang.IllegalArgumentException("No configs match configSpec");
			}

			/* Allocate then read the array of minimally matching EGL configs
             */
			EGLConfig[] configs = new EGLConfig[numConfigs];
			egl.EglChooseConfig(display, s_configAttribs2, configs, numConfigs, num_config);

			//if (DEBUG) {
			//	printConfigs(egl, display, configs);
			//}
			/* Now return the "best" one
             */
			return ChooseConfig(egl, display, configs);
		}

		public EGLConfig ChooseConfig(IEGL10 egl, Javax.Microedition.Khronos.Egl.EGLDisplay display,
			Javax.Microedition.Khronos.Egl.EGLConfig[] configs) {
			foreach(EGLConfig config in configs) {
				int d = findConfigAttrib(egl, display, config,
					EGL10.EglDepthSize, 0);
				int s = findConfigAttrib(egl, display, config,
					EGL10.EglStencilSize, 0);

				// We need at least mDepthSize and mStencilSize bits
				if (d < mDepthSize || s < mStencilSize)
					continue;

				// We want an *exact* match for red/green/blue/alpha
				int r = findConfigAttrib(egl, display, config,
					EGL10.EglRedSize, 0);
				int g = findConfigAttrib(egl, display, config,
					12323, 0);
				int b = findConfigAttrib(egl, display, config,
					EGL10.EglBlueSize, 0);
				int a = findConfigAttrib(egl, display, config,
					EGL10.EglAlphaSize, 0);

				if (r == mRedSize && g == mGreenSize && b == mBlueSize && a == mAlphaSize)
					return config;
			}
			return null;
		}

		private int findConfigAttrib(IEGL10 egl, EGLDisplay display,
			EGLConfig config, int attribute, int defaultValue) {

			if (egl.EglGetConfigAttrib(display, config, attribute, mValue)) {
				return mValue[0];
			}
			return defaultValue;
		}

		private void printConfigs(IEGL10 egl, EGLDisplay display,
			EGLConfig[] configs) {
			int numConfigs = configs.Length;
			Log.Warn(EmuSurfaceView.TAG, String.Format("%d configurations", numConfigs));
			for (int i = 0; i < numConfigs; i++) {
				Log.Warn(EmuSurfaceView.TAG, String.Format("Configuration %d:\n", i));
				printConfig(egl, display, configs[i]);
			}
		}

		private void printConfig(IEGL10 egl, EGLDisplay display,
			EGLConfig config) {
			int[] attributes = {
				EGL10.EglBufferSize,
				EGL10.EglAlphaSize,
				EGL10.EglBlueSize,
				EGL10.EglGreenSize,
				EGL10.EglRedSize,
				EGL10.EglDepthSize,
				EGL10.EglStencilSize,
				EGL10.EglConfigCaveat,
				EGL10.EglConfigId,
				EGL10.EglLevel,
				EGL10.EglMaxPbufferHeight,
				EGL10.EglMaxPbufferPixels,
				EGL10.EglMaxPbufferWidth,
				EGL10.EglNativeRenderable,
				EGL10.EglNativeVisualId,
				EGL10.EglNativeVisualType,
				0x3030, // EGL10.EGL_PRESERVED_RESOURCES,
				EGL10.EglSamples,
				EGL10.EglSampleBuffers,
				EGL10.EglSurfaceType,
				EGL10.EglTransparentType,
				EGL10.EglTransparentRedValue,
				EGL10.EglTransparentGreenValue,
				EGL10.EglTransparentBlueValue,
				0x3039, // EGL10.EGL_BIND_TO_TEXTURE_RGB,
				0x303A, // EGL10.EGL_BIND_TO_TEXTURE_RGBA,
				0x303B, // EGL10.EGL_MIN_SWAP_INTERVAL,
				0x303C, // EGL10.EGL_MAX_SWAP_INTERVAL,
				EGL10.EglLuminanceSize,
				EGL10.EglAlphaMaskSize,
				EGL10.EglColorBufferType,
				EGL10.EglRenderableType,
				0x3042 // EGL10.EGL_CONFORMANT
			};
			String[] names = {
				"EGL_BUFFER_SIZE",
				"EGL_ALPHA_SIZE",
				"EGL_BLUE_SIZE",
				"EGL_GREEN_SIZE",
				"EGL_RED_SIZE",
				"EGL_DEPTH_SIZE",
				"EGL_STENCIL_SIZE",
				"EGL_CONFIG_CAVEAT",
				"EGL_CONFIG_ID",
				"EGL_LEVEL",
				"EGL_MAX_PBUFFER_HEIGHT",
				"EGL_MAX_PBUFFER_PIXELS",
				"EGL_MAX_PBUFFER_WIDTH",
				"EGL_NATIVE_RENDERABLE",
				"EGL_NATIVE_VISUAL_ID",
				"EGL_NATIVE_VISUAL_TYPE",
				"EGL_PRESERVED_RESOURCES",
				"EGL_SAMPLES",
				"EGL_SAMPLE_BUFFERS",
				"EGL_SURFACE_TYPE",
				"EGL_TRANSPARENT_TYPE",
				"EGL_TRANSPARENT_RED_VALUE",
				"EGL_TRANSPARENT_GREEN_VALUE",
				"EGL_TRANSPARENT_BLUE_VALUE",
				"EGL_BIND_TO_TEXTURE_RGB",
				"EGL_BIND_TO_TEXTURE_RGBA",
				"EGL_MIN_SWAP_INTERVAL",
				"EGL_MAX_SWAP_INTERVAL",
				"EGL_LUMINANCE_SIZE",
				"EGL_ALPHA_MASK_SIZE",
				"EGL_COLOR_BUFFER_TYPE",
				"EGL_RENDERABLE_TYPE",
				"EGL_CONFORMANT"
			};
			int[] value = new int[1];
			for (int i = 0; i < attributes.Length; i++) {
				int attribute = attributes[i];
				String name = names[i];
				if ( egl.EglGetConfigAttrib(display, config, attribute, value)) {
					Log.Warn(EmuSurfaceView.TAG, String.Format("  %s: %d\n", name, value[0]));
				} else {
					// Log.w(TAG, String.format("  %s: failed\n", name));
					while (egl.EglGetError() != EGL10.EglSuccess);
				}
			}
		}

		// Subclasses can adjust these values:
		protected int mRedSize;
		protected int mGreenSize;
		protected int mBlueSize;
		protected int mAlphaSize;
		protected int mDepthSize;
		protected int mStencilSize;
		private int[] mValue = new int[1];
	}
}


﻿using System;

using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.ES11;
using OpenTK.Platform;
using OpenTK.Platform.Android;

using Android.Views;
using Android.Content;
using Android.Util;
using System.Runtime.InteropServices;
using Android.Opengl;
using Javax.Microedition.Khronos.Opengles;
using Java.Lang;
using System.IO;
using Android.Graphics;
using System.IO.IsolatedStorage;
using System.Security.Cryptography;
using Android.App;
using MogaMoga;


namespace NesOneAndroid
{
	public class TestOpenGL{
		[DllImport("libNesOneComp.so")] 
		public  static extern void on_surface_created();
		[DllImport("libNesOneComp.so")] 
		public  static extern void on_surface_changed(int w, int h);
		[DllImport("libNesOneComp.so")] 
		public  static extern void on_draw_frame();
		[DllImport("libNesOneComp.so")] 
		public  static extern void init();
		[DllImport("libNesOneComp.so")] 
		public  static extern void reset();
		[DllImport("libNesOneComp.so")] 
		public  static extern void load_rom_file (string filename, bool resume);
		[DllImport("libNesOneComp.so")] 
		public  static extern void save_state (string filename);
		[DllImport("libNesOneComp.so")] 
		public  static extern void load_state (string filename);
		[DllImport("libNesOneComp.so")] 
		public  static extern void update(double timeDelta);
		[DllImport("libNesOneComp.so")] 
		public  static extern void create_button(int[] pixels, int w, int h, float xPos, float yPos, int buttonID);
		[DllImport("libNesOneComp.so")] 
		public  static extern void remove_all_buttons();
		[DllImport("libNesOneComp.so")] 
		public  static extern void handle_touch(int id, int type, float x, float y);
		[DllImport("libNesOneComp.so")] 
		public  static extern void pause();
		[DllImport("libNesOneComp.so")] 
		public  static extern void resume();
		[DllImport("libNesOneComp.so")] 
		public  static extern void set_emulator_sound(bool on);
		[DllImport("libNesOneComp.so")] 
		public  static extern void init_sound_setting(bool on, int sampleRate);
		[DllImport("libNesOneComp.so")] 
		public  static extern void init_video_setting(bool autodetect, int system);
		[DllImport("libNesOneComp.so")] 
		public  static extern void init_general_setting(bool abButton, int orientation);
		[DllImport("libNesOneComp.so")] 
		public  static extern void set_paths(string snaps, string autosavePath, string cheatFolder);
		[DllImport("libNesOneComp.so")] 
		public  static extern IntPtr decode_game_genie_code(string code);
		[DllImport("libNesOneComp.so")] 
		public  static extern void release_array_memory(IntPtr array);
		[DllImport("libNesOneComp.so")]
		public  static extern void set_controller_data(int data);
		[DllImport("libNesOneComp.so")]
		public  static extern void set_play_speed(int speed);
	}

	public class EmulatorRenderer : Java.Lang.Object, GLSurfaceView.IRenderer
	{
		Controller _controller = null;

		bool _firstLoop, _romloaded;
		double _lastTime, _deltaTime, _currentTime;
		float[] proj = new float[16];
		string _romPath;
		float _buttonScale = 1.0f;

		public bool IsROMLoaded{
			get{
				return _romloaded;
			}
		}




		public EmulatorRenderer(){


			Init();
		}


		public void RunROM(string romPath, bool resume){

			//InitMogaWithIntentFix ();

			if (File.Exists (romPath)) {
				_romPath = romPath;
				InitializeROMSetting (romPath);

				TestOpenGL.load_rom_file (romPath, resume);

				/*var saveStateFile = System.IO.Path.Combine (Settings.SaveStateFolder, Utilities.Renderer.GetCurrentROMHash + "0.sav");
				if (File.Exists (saveStateFile)) {
					LoadAutoSaveState (File.GetLastWriteTime (saveStateFile).ToString ());
				} else {
					TestOpenGL.load_rom_file (romPath, false);
				}*/
				_romloaded = true;
			}
		}

		private void LoadAutoSaveState(string date){
			var context= Application.Context;
			AlertDialog.Builder builder;
			builder = new AlertDialog.Builder(context);
			builder.SetTitle("Load ROM");
			builder.SetMessage ("Autosave state from " + date + " found. Do you want to continue?");
			builder.SetCancelable(false);
			builder.SetPositiveButton("Yes", delegate { TestOpenGL.load_rom_file (_romPath, true); });
			builder.SetNegativeButton("No", delegate { TestOpenGL.load_rom_file (_romPath, false); });
			builder.Show();
		}


		public void Init(){
			_lastTime = _deltaTime = _currentTime = 0;
			_firstLoop = true;
			_romloaded = false;
			TestOpenGL.init ();
			InitializeEmulatorSettings ();
		}

		public void Reset(){
			TestOpenGL.reset ();
		}

		public void SetSpeed(int speed){
			TestOpenGL.set_play_speed (speed);
			ResumeEmulation ();
		}
			

		private void CreateButtons ()
		{
			switch (Settings.ButtonSize.ToLower ()) {
			case "small":
				_buttonScale = 0.75f;
				break;
			case "large":
				_buttonScale = 1.2f;
				break;
			default:
				_buttonScale = 1.0f;
				break;
			}


			Utilities.LoadButtonPositions (Utilities.RootActivity);
			TestOpenGL.remove_all_buttons ();
			Point ptD, ptStart, ptSelect, ptA, ptB;
			if (string.Compare (Settings.Orientation, "portrait", true) == 0) {
				if (Settings.ABButtonEnabled) {
					LoadButton ("buttons/" + Settings.ButtonStyle.ToLower () + "/ab.png", Settings.ABButtonPortraitPosition.X, Settings.ABButtonPortraitPosition.Y, (int)(NesButtons.JOY_A | NesButtons.JOY_B));
				}

				ptA = Settings.AButtonPortraitPosition;
				ptB = Settings.BButtonPortraitPosition;
				ptStart = Settings.StartButtonPortraitPosition;
				ptSelect = Settings.SelectButtonPortraitPosition;
				ptD = Settings.DirectionButtonPortraitPosition;

			} else {

				if (Settings.ABButtonEnabled) {
					LoadButton ("buttons/" + Settings.ButtonStyle.ToLower () + "/ab.png", Settings.ABButtonLandscapePosition.X, Settings.ABButtonLandscapePosition.Y, (int)(NesButtons.JOY_A | NesButtons.JOY_B));
				}

				ptA = Settings.AButtonLandscapePosition;
				ptB = Settings.BButtonLandscapePosition;
				ptStart = Settings.StartButtonLandscapePosition;
				ptSelect = Settings.SelectButtonLandscapePosition;
				ptD = Settings.DirectionButtonLandscapePosition;
			}


			LoadButton ("buttons/" + Settings.ButtonStyle.ToLower () + "/a.png", ptA.X, ptA.Y, (int)NesButtons.JOY_A);
			LoadButton ("buttons/" + Settings.ButtonStyle.ToLower () + "/b.png", ptB.X, ptB.Y, (int)NesButtons.JOY_B);

			LoadButton ("buttons/" + Settings.ButtonStyle.ToLower () + "/start.png", ptStart.X, ptStart.Y, (int)NesButtons.JOY_START);
			LoadButton ("buttons/" + Settings.ButtonStyle.ToLower () + "/select.png", ptSelect.X, ptSelect.Y, (int)NesButtons.JOY_SELECT);


			LoadButton ("buttons/" + Settings.ButtonStyle.ToLower () + "/direction.png", ptD.X, ptD.Y, 0);
			var dButtonSpacing = 80.0f * _buttonScale;
			LoadButton ("buttons/blank80x80.png", ptD.X + dButtonSpacing, ptD.Y, (int)NesButtons.JOY_UP);
			LoadButton ("buttons/blank80x80.png", ptD.X + dButtonSpacing, ptD.Y + dButtonSpacing * 2, (int)NesButtons.JOY_DOWN);
			LoadButton ("buttons/blank80x80.png", ptD.X, ptD.Y + dButtonSpacing, (int)NesButtons.JOY_LEFT);
			LoadButton ("buttons/blank80x80.png", ptD.X + dButtonSpacing * 2, ptD.Y + dButtonSpacing, (int)NesButtons.JOY_RIGHT);

			LoadButton ("buttons/blank80x80.png", ptD.X, ptD.Y, (int)(NesButtons.JOY_UP | NesButtons.JOY_LEFT));
			LoadButton ("buttons/blank80x80.png", ptD.X + dButtonSpacing * 2, ptD.Y, (int)(NesButtons.JOY_UP | NesButtons.JOY_RIGHT));
			LoadButton ("buttons/blank80x80.png", ptD.X + dButtonSpacing * 2, ptD.Y + dButtonSpacing * 2, (int)(NesButtons.JOY_DOWN | NesButtons.JOY_RIGHT));
			LoadButton ("buttons/blank80x80.png", ptD.X, ptD.Y + dButtonSpacing * 2, (int)(NesButtons.JOY_DOWN | NesButtons.JOY_LEFT));

		}

		private void LoadButton(string filePath, float x, float y, int buttonID){
			var context = Android.App.Application.Context;
			var bitmap = BitmapFactory.DecodeStream (context.Assets.Open (filePath));

			int width = Convert.ToInt32 ((float)bitmap.Width * _buttonScale);
			int height = Convert.ToInt32 ((float)bitmap.Height * _buttonScale);

			bitmap = Bitmap.CreateScaledBitmap (bitmap, width, height, false);
			var imageBytes = new int[bitmap.Width * bitmap.Height];
			bitmap.GetPixels (imageBytes, 0, bitmap.Width, 0, 0, bitmap.Width, bitmap.Height);

			TestOpenGL.create_button(imageBytes,  bitmap.Width, bitmap.Height, x, y, buttonID );
		}


		public void OnSurfaceCreated(IGL10 gl, Javax.Microedition.Khronos.Egl.EGLConfig config) {
			TestOpenGL.on_surface_created();
		}


		public void OnSurfaceChanged(IGL10 gl, int width, int height) {
			TestOpenGL.on_surface_changed (width, height);
			CreateButtons ();
		}

		public void OnDrawFrame(IGL10 gl) {

			_currentTime = JavaSystem.CurrentTimeMillis();
			_deltaTime = _currentTime - _lastTime;
			if (_romloaded) {
				if (!_firstLoop) {
					CreateKeyMaskFromMoga ();
					TestOpenGL.update (_deltaTime / 1000.0);
				} else {
					_firstLoop = false;
				}
			}
			_lastTime = _currentTime;
		}

		public void HandleTouch(Android.Views.MotionEvent e){
			var pointerIndex = e.ActionIndex;			
			int pointerId = e.GetPointerId (pointerIndex);
			if (pointerId > 3) {
				return;
			}
			var touchAction = e.ActionMasked;

			switch (touchAction) {
			case MotionEventActions.Down:
			case MotionEventActions.PointerDown:
				TestOpenGL.handle_touch (pointerId, 0, e.GetX (pointerIndex), e.GetY (pointerIndex));
				break;
			case MotionEventActions.Move:
				var pointCount = e.PointerCount;
				for (int i = 0; i < pointCount; i++) {
					int movePointerId = e.GetPointerId (i);
					TestOpenGL.handle_touch (movePointerId, 1, e.GetX (i), e.GetY (i));
				}
				break;
			case MotionEventActions.Up:
			case MotionEventActions.Cancel:
			case MotionEventActions.PointerUp:
				TestOpenGL.handle_touch (pointerId, 2, e.GetX (pointerIndex), e.GetY (pointerIndex));
				break;
			}

		}

		public void PauseEmulation(){
			SaveState ("0");
			TestOpenGL.pause ();
		}

		public void ResumeEmulation(){
			TestOpenGL.resume ();
		}

		public void SaveState(string slotNumber){
			if (_romPath != null && _romloaded) {
				var filename = System.IO.Path.GetFileNameWithoutExtension (_romPath);
				var path = System.IO.Path.Combine (Settings.SaveStateFolder, GetFileHash(_romPath) + slotNumber.ToString() + ".sav");
				TestOpenGL.save_state (path);
			}
		}

		public void LoadState(string slotNumber){
			if (_romPath != null && _romloaded) {
				var filename = System.IO.Path.GetFileNameWithoutExtension (_romPath);
				var path = System.IO.Path.Combine (Settings.SaveStateFolder, GetFileHash(_romPath) + slotNumber.ToString() + ".sav");
				TestOpenGL.load_state (path);
			}
		}

		void CreateKeyMaskFromMoga(){
			var controller = Utilities.RootActivity.MogaController;
			int mask = 0;

			if (controller!= null && controller.GetState (Controller.StateConnection) == Controller.ActionConnected) {
				if (controller.GetKeyCode (Controller.KeycodeButtonA) == Controller.ActionDown) {
					mask |= (int)NesButtons.JOY_A;
				}
				if (controller.GetKeyCode (Controller.KeycodeButtonStart) == Controller.ActionDown) {
					mask |= (int)NesButtons.JOY_START;
				}

				TestOpenGL.set_controller_data (mask);
			}


		}


		public void Destroy(){
			if (_controller != null) {
				_controller.Exit ();
			}
		}

		public void Pause(){
			if (_controller != null) {
				_controller.OnPause ();
			}
		}

		public void Resume(){
			if (_controller != null) {
				_controller.OnResume ();
			}
		}



		public static void InitializeEmulatorSettings(){
			TestOpenGL.init_sound_setting (Settings.SoundOn, Convert.ToInt32(Settings.SampleRate));

			var vSystem = (string.Compare (Settings.VideoSystem, "NTSC") == 0) ? 0 : 1;

			TestOpenGL.init_video_setting (Settings.VideoAutodetect, vSystem);

			var orientation = (string.Compare (Settings.Orientation, "Portrait") == 0) ? 0 : 1;
			TestOpenGL.init_general_setting (Settings.ABButtonEnabled, orientation);
		}

		static void InitializeROMSetting (string romPath)
		{
			var filename = System.IO.Path.GetFileNameWithoutExtension (romPath);
			Settings.CurrentROMSnapshotPath = System.IO.Path.Combine (Settings.SnapshotFolder, filename + ".img");

			var saveStateFile = System.IO.Path.Combine (Settings.SaveStateFolder, GetFileHash(romPath) + "0.sav");

			TestOpenGL.set_paths (Settings.CurrentROMSnapshotPath, saveStateFile, Settings.CheatsFolder);
		}


		public static string GetFileHash(string path){
			using (FileStream stream = File.OpenRead(path))
			{
				var md5 = SHA1.Create();
				byte[] hash = md5.ComputeHash(stream);
				return BitConverter.ToString(hash).Replace("-", string.Empty);
			}
		}

	}
}

